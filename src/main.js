import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import First from './components/Routers/First.vue'
import Second from './components/Routers/Second.vue'
import User from './components/Routers/User.vue'
import Homeroute from './components/Routers/Homeroute.vue'

Vue.use(VueRouter);

const routes = [
  { path: '/', component: First },
  { path: '/Second', component: Second },
  { path: '/Home', component: Homeroute },
  { path: '/User/:id', component: User }
]
 
const router = new VueRouter({
  routes
});
Vue.config.productionTip = false

Vue.filter("Ucase", function (val) {
  return val.toUpperCase();
})

Vue.filter("currencyConvertor", function (val) {
  return val * 100;
})

new Vue({
  router:router,
  render: h => h(App),
}).$mount('#app')
